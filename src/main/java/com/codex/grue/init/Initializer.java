package com.codex.grue.init;

import com.codex.grue.entity.Author;
import com.codex.grue.entity.Book;
import com.codex.grue.entity.Publisher;
import com.codex.grue.repository.AuthorRepository;
import com.codex.grue.repository.BookRepository;
import com.codex.grue.repository.PublisherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Initializer implements CommandLineRunner {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final PublisherRepository publisherRepository;

    @Override
    public void run(String... args) throws Exception {
        Author max = new Author();
        max.setFirstName("max");
        max.setLastName("dev");

        Publisher publisher = new Publisher();
        publisher.setPublisherName("king");
        publisher.setAddress("usa");
        publisher.setZip("515");
        publisher.setState("dc");
        publisher.setCity("ny");
        Publisher saved_king = publisherRepository.save(publisher);


        Book ddd = new Book();
        ddd.setTitle("domain driven design");
        ddd.setIsbn("123456");
        Book savedDDD = bookRepository.save(ddd);
        savedDDD.setPublisher(saved_king);
        Author savedMax = authorRepository.save(max);
        savedMax.getBooks().add(savedDDD);
         authorRepository.save(savedMax);
        bookRepository.save(savedDDD);
    }
}
